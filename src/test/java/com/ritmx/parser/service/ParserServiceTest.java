package com.ritmx.parser.service;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.ritmx.parser.entity.ProgramLog;
import com.ritmx.parser.entity.Server;
import com.ritmx.util.ServerStatus;

@RunWith(MockitoJUnitRunner.class)
public class ParserServiceTest {

	ParserService parserService;

	@Before
	public void init() {
		parserService = new ParserServiceImpl();
	}

	@Test
	public void getServersConnectedToTest() {
		Server serverA = new Server("a", ServerStatus.IN);
		Server serverB = new Server("b", ServerStatus.OUT);
		Server serverC = new Server("c", ServerStatus.OUT);
		List<Server> serversOut = new ArrayList<Server>();
		serversOut.add(serverB);
		serversOut.add(serverC);
		ProgramLog programLog = new ProgramLog(15, serverA, serversOut);
		List<ProgramLog> programLogs = new ArrayList<ProgramLog>();
		programLogs.add(programLog);
		List<ProgramLog> parserList = parserService.getServersConnectedTo(15, programLogs);
		assertEquals(parserList.get(0).getServer().getName(), "a");
	}

	@Test
	public void getServersWhoServerConnectsToTest() {
		Server serverA = new Server("a", ServerStatus.OUT);
		Server serverB = new Server("b", ServerStatus.IN);
		Server serverC = new Server("c", ServerStatus.IN);
		List<Server> serversOut = new ArrayList<Server>();
		serversOut.add(serverB);
		serversOut.add(serverC);
		ProgramLog programLog = new ProgramLog(15, serverA, serversOut);
		List<ProgramLog> programLogs = new ArrayList<ProgramLog>();
		programLogs.add(programLog);
		List<ProgramLog> parserList = parserService.getServersWhoServerConnectsTo(15, programLogs);
		assertEquals(parserList.get(0).getServer().getName(), "a");
	}

	@Test()
	public void serverHaveMaxConnectionTest() {
		Server serverA = new Server("a", ServerStatus.IN);
		Server serverB = new Server("b", ServerStatus.IN);
		Server serverC = new Server("c", ServerStatus.OUT);
		List<Server> serversOutA = new ArrayList<Server>();
		serversOutA.add(serverC);
		ProgramLog programLogA = new ProgramLog(15, serverA, serversOutA);
		List<Server> serversOutB = new ArrayList<Server>();
		serversOutB.add(serverC);
		serversOutB.add(serverC);
		ProgramLog programLogB = new ProgramLog(15, serverB, serversOutB);
		List<ProgramLog> programLogs = new ArrayList<ProgramLog>();
		programLogs.add(programLogA);
		programLogs.add(programLogB);
		String message = parserService.getServerHaveMaxConnection(15, programLogs);
		assertEquals(message, "b");
	}

}
