package com.ritmx.util;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import com.ritmx.parser.entity.ProgramLog;
import com.ritmx.parser.entity.Server;

public class ServerUtil {

	public static Map<Integer, List<ProgramLog>> createServersByHourFromStream(Stream<String> stream) {
		Map<Integer, List<ProgramLog>> serversByHour = new HashMap<Integer, List<ProgramLog>>();
		stream.forEach(line -> {
			String[] splittedLine = line.split(" ");
			LocalDateTime timeStamp = DateUtil.fromLongToLocalDateTime(Long.valueOf(splittedLine[0]));
			Integer hour = timeStamp.getHour();
			String serverInName = splittedLine[1];
			String serverOutName = splittedLine[2];
			Server serverIn = new Server(serverInName, ServerStatus.IN);
			Server serverOut = new Server(serverOutName, ServerStatus.OUT);
			if (serversByHour.containsKey(hour)) {
				if (serversByHour.get(hour).stream().anyMatch(match -> serverIn.equals(match.getServer()))) {
					serversByHour.get(hour).stream()
							.filter(programLogFilter -> serverIn.equals(programLogFilter.getServer()))
							.forEach(program -> {
								program.getServerList().add(serverOut);
							});
				} else {
					List<Server> serversOut = new ArrayList<Server>();
					serversOut.add(serverOut);
					ProgramLog programLog = new ProgramLog(hour, serverIn, serversOut);
					serversByHour.get(hour).add(programLog);
				}
				if (serversByHour.get(hour).stream().anyMatch(match -> serverOut.equals(match.getServer()))) {
					serversByHour.get(hour).stream()
							.filter(programLogFilter -> serverOut.equals(programLogFilter.getServer()))
							.forEach(program -> {
								program.getServerList().add(serverIn);
							});
				} else {
					List<Server> serversIn = new ArrayList<Server>();
					serversIn.add(serverIn);
					ProgramLog programLog = new ProgramLog(hour, serverOut, serversIn);
					serversByHour.get(hour).add(programLog);
				}
			} else {
				List<Server> serversOut = new ArrayList<Server>();
				serversOut.add(serverOut);
				ProgramLog programLogIn = new ProgramLog(hour, serverIn, serversOut);
				List<Server> serversIn = new ArrayList<Server>();
				serversOut.add(serverIn);
				ProgramLog programLogOut = new ProgramLog(hour, serverOut, serversIn);
				List<ProgramLog> programLogList = new ArrayList<ProgramLog>();
				programLogList.add(programLogIn);
				programLogList.add(programLogOut);
				serversByHour.put(hour, programLogList);
			}
		});
		return serversByHour;
	}

}
