package com.ritmx.util;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class DateUtil {

	public static LocalDateTime fromLongToLocalDateTime(long dateAsLong) {
		LocalDateTime dateTime =
			    LocalDateTime.ofInstant(Instant.ofEpochSecond(dateAsLong), ZoneId.systemDefault());
		return dateTime;
	}

}
