package com.ritmx.parser.entity;

import java.util.List;

public class ProgramLog {
	Integer hourOfConnection;
	Server server;
	List<Server> serverList;
	
	
	/**
	 * @param hourOfConnection
	 * @param server
	 * @param serverList
	 */
	public ProgramLog(Integer hourOfConnection, Server server, List<Server> serverList) {
		super();
		this.hourOfConnection = hourOfConnection;
		this.server = server;
		this.serverList = serverList;
	}
	/**
	 * @return the hourOfConnection
	 */
	public Integer getHourOfConnection() {
		return hourOfConnection;
	}
	/**
	 * @param hourOfConnection the hourOfConnection to set
	 */
	public void setHourOfConnection(Integer hourOfConnection) {
		this.hourOfConnection = hourOfConnection;
	}
	/**
	 * @return the server
	 */
	public Server getServer() {
		return server;
	}
	/**
	 * @param server the server to set
	 */
	public void setServer(Server server) {
		this.server = server;
	}
	/**
	 * @return the serverList
	 */
	public List<Server> getServerList() {
		return serverList;
	}
	/**
	 * @param serverList the serverList to set
	 */
	public void setServerList(List<Server> serverList) {
		this.serverList = serverList;
	}
}
