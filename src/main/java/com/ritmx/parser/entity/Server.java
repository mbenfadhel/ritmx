package com.ritmx.parser.entity;

import com.ritmx.util.ServerStatus;

public class Server {
	
	private String name;
	private ServerStatus serverStatus;
	
	/**
	 * @param name
	 * @param serverStatus
	 */
	public Server(String name, ServerStatus serverStatus) {
		super();
		this.name = name;
		this.serverStatus = serverStatus;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}



	/**
	 * @return the serverStatus
	 */
	public ServerStatus getServerStatus() {
		return serverStatus;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((serverStatus == null) ? 0 : serverStatus.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Server other = (Server) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (serverStatus != other.serverStatus)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Server [name=" + name + ", serverStatus=" + serverStatus + "]";
	}
}
