package com.ritmx.parser.main;

import java.util.List;
import java.util.Map;

import com.ritmx.parser.entity.ProgramLog;
import com.ritmx.parser.service.ParserServiceImpl;
import com.ritmx.util.ServerUtil;

public class MainParser {

	public static void main(String[] args) {
		ParserServiceImpl parserService = new ParserServiceImpl();
		Map<Integer, List<ProgramLog>> serversByHour = ServerUtil
				.createServersByHourFromStream(parserService.getStream());
		for (Integer hour : serversByHour.keySet()) {
			List<ProgramLog> allPRograms = serversByHour.get(hour);
			System.out.println("**************** Heure:" + hour + "h  ********************");
			// 1
			System.out.println("****************1****************");
			List<ProgramLog> serversConnectedTo = parserService.getServersConnectedTo(hour, allPRograms);
			serversConnectedTo.forEach(fetched -> {
				System.out.println("****la liste des serveurs qui ont été connecté à " + fetched.getServer().getName()
						+ " durant cette heure:");
				fetched.getServerList().stream().map(mapper -> mapper.getName()).forEach(System.out::println);
			});
			// 2
			System.out.println("****************2****************");
			List<ProgramLog> serversWhoServerConnectsTo = parserService.getServersWhoServerConnectsTo(hour,
					allPRograms);
			serversWhoServerConnectsTo.forEach(fetched -> {
				System.out.println("****la liste des serveurs auxquels " + fetched.getServer().getName()
						+ " s'est connecté durant cette heure:");
				fetched.getServerList().stream().map(mapper -> mapper.getName()).forEach(System.out::println);
			});
			// 3
			System.out.println("****************3****************");
			String ServerHaveMaxConnection = parserService.getServerHaveMaxConnection(hour, allPRograms);
			System.out.println(
					"le serveur qui a généré le plus de connections durant cette heure => " + ServerHaveMaxConnection);

		}

	}

}
