package com.ritmx.parser.service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ritmx.parser.entity.ProgramLog;
import com.ritmx.util.ServerStatus;

public class ParserServiceImpl implements ParserService{
	private static final Logger LOG = LogManager.getLogger(ParserServiceImpl.class);

	private String fileName;
	private Stream<String> stream;
	private String maxConnectedServer = "";
	private int numberOfConnection = 0;

	private Properties prop = new Properties();
	private InputStream input = null;

	public ParserServiceImpl() {
		input = getClass().getClassLoader().getResourceAsStream("config.properties");
		// load a properties file
		try {
			prop.load(input);
			fileName = prop.getProperty("filename");
			stream = Files.lines(Paths.get(fileName));
		} catch (IOException e) {
			LOG.error(e.getMessage());
		}
	}

	public Stream<String> getStream() {
		return stream;
	}

	public List<ProgramLog> getServersConnectedTo(Integer hour, List<ProgramLog> programLogs) {
		return programLogs.stream()
				.filter(filteredProgram -> hour.equals(hour)
						&& ServerStatus.IN.equals(filteredProgram.getServer().getServerStatus()))
				.collect(Collectors.toList());
	}

	public List<ProgramLog> getServersWhoServerConnectsTo(Integer hour, List<ProgramLog> programLogs) {
		return programLogs.stream()
				.filter(filteredProgram -> hour.equals(hour)
						&& ServerStatus.OUT.equals(filteredProgram.getServer().getServerStatus()))
				.collect(Collectors.toList());
	}

	public String getServerHaveMaxConnection(int hour, List<ProgramLog> programLogs) {
		numberOfConnection = 0;
		maxConnectedServer = "";
		List<ProgramLog> programLogsIn = getServersConnectedTo(hour, programLogs);
		programLogsIn.forEach(program -> {
			if (program.getServerList().size() > numberOfConnection) {
				numberOfConnection = program.getServerList().size();
				maxConnectedServer = program.getServer().getName();
			}
		});
		return maxConnectedServer;
	}

}
