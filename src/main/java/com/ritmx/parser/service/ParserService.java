package com.ritmx.parser.service;

import java.util.List;

import com.ritmx.parser.entity.ProgramLog;

public interface ParserService {

	/**
	 * la liste des serveurs qui ont été connecté à un serveur donné durant cette
	 * heure
	 * 
	 * @param hour
	 * @param programLogs
	 * @return
	 */
	public List<ProgramLog> getServersConnectedTo(Integer hour, List<ProgramLog> programLogs);

	/**
	 * la liste des serveurs auxquels un serveur donné s'est connecté durant cette
	 * heure
	 * 
	 * @param hour
	 * @param programLogs
	 * @return
	 */
	public List<ProgramLog> getServersWhoServerConnectsTo(Integer hour, List<ProgramLog> programLogs);

	/**
	 * le serveur qui a généré le plus de connections durant cette heure
	 * 
	 * @param hour
	 * @param programLogs
	 * @return
	 */
	public String getServerHaveMaxConnection(int hour, List<ProgramLog> programLogs);

}
